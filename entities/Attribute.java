package com.latam.transfer.comarch.entities;

import lombok.Data;

@Data
public class Attribute {

	String code;
	String value;
}
