package com.latam.transfer.comarch.entities;

import lombok.Data;

@Data
public class AttributeRequest {

	String code;
	String value;
}
