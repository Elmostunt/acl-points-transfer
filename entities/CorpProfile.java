package com.latam.transfer.comarch.entities;

import java.util.List;

import lombok.Data;

@Data
public class CorpProfile {

	Integer accountId;
	List<Attribute> attributes;
	Integer companyId;
	Integer id;
	String lastModify;	
	String no;
	Boolean redemptionEnabled;
	String status;
	String statusName;
	String type;
}
