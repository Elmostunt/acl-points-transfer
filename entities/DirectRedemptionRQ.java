package com.latam.transfer.comarch.entities;

import java.util.List;

import lombok.Data;
import lombok.NonNull;

@Data
@NonNull
public class DirectRedemptionRQ {
	
	int points;
	String partner;
	Boolean useLoan; 
	String pointType;
	String comment;
	Boolean bookTransaction;
	List<AttributeRequest> attributes;

}
