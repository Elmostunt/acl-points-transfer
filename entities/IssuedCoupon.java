package com.latam.transfer.comarch.entities;

import java.sql.Date;

import lombok.Data;
@Data
public class IssuedCoupon {

	String couponNumber;
	Date expiryDate;
	Integer id;
	String type;
	String typeName;
	
}
