package com.latam.transfer.comarch.entities;

import lombok.Data;

@Data
public class PointBalance {
	String expirationDate;
	String pointType;
	String pointTypeName;
	Integer points;
	String status;
	String statusName;
//    {
//        "expirationDate": "2020-04-30",
//        "pointType": "PT1",
//        "pointTypeName": "LATAM Points",
//        "points": 6000,
//        "status": "B",
//        "statusName": "Booked"
//    }
}
