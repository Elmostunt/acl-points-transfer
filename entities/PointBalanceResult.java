package com.latam.transfer.comarch.entities;

import lombok.Data;

@Data
public class PointBalanceResult {
	String pointType;
	Integer points;
}
