package com.latam.transfer.comarch.entities;

import java.util.List;

import lombok.Data;
@Data
public class RefTransactionResult {
	
	Integer balance;
	Integer loanPoints;
	Integer bonusPoints;
	String message;
	Integer moneyAmount;
	List<PointBalanceResult> pointBalances;
	List<PointBalanceResult> pointTypes;
	Integer points;
	Integer transactionId;
	Integer orderId;
	Integer moneyDiscount;
	List<PointBalanceResult> bonusPointTypes;
	List<PointBalanceResult> loanPointTypes;
	Boolean blockUser;
	Boolean blockCustomer;
	Boolean blockAccount;
	List<IssuedCoupon> issuedCoupons;
}
