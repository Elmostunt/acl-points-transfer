package com.latam.transfer.acl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.latam.transfer.acl")
public class AclPointsTransferApplication {

	public static void main(String[] args) {
		SpringApplication.run(AclPointsTransferApplication.class, args);
	}

}
