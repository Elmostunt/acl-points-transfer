package com.latam.transfer.acl.comarch.entities;

import lombok.Data;

@Data
public class Attribute {

	String code;
	String value;
}
