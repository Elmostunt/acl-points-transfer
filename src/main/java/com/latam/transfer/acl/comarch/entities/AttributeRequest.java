package com.latam.transfer.acl.comarch.entities;

import lombok.Data;

@Data
public class AttributeRequest {

	String code;
	String value;
}
