package com.latam.transfer.acl.comarch.entities;

import java.util.List;

import lombok.Data;

@Data
public class BalanceRS {

	List<PointBalance> pointBalances;
	Integer totalPointBalance;
	Integer waitingToBookPointBalance;
}
