package com.latam.transfer.acl.comarch.entities;

import java.util.List;

public class DirectRedemptionRQDTO {

	int customerId;	
	int points;
	String partner;
	Boolean useLoan; 
	String pointType;
	String comment;
	Boolean bookTransaction;
	List<AttributeRequest> attributes;
}
