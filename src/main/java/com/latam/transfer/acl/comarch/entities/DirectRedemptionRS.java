package com.latam.transfer.acl.comarch.entities;

import java.util.List;

import lombok.Data;

@Data
public class DirectRedemptionRS {

	//TODO Complementar RS
	Integer balance;
	Boolean blockUser;
	Boolean blockCustomer;
	Boolean blockAccount;
	Integer bonusPoints;
	List<IssuedCoupon> issuedCoupons;
	Integer loanPoints;
	String message;
	Integer moneyAmount;
	List<PointBalanceResult> pointBalances;
	List<PointBalanceResult> pointTypes;
	
	Integer transactionId;
	Integer points;
	PointBalanceResult pointBalanceResult;

	List<PointBalanceResult> bonusPointTypes;
	List<PointBalanceResult> loanPointTypes;
	RefTransactionResult refTransactionResult;


	List<UsedCoupon> usedCoupon;
	Integer expiratedPoint;
	List<PointBalanceResult> expiratedPointTypes;
	List<CounterHistory> counters;
}
