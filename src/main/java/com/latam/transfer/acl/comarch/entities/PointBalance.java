package com.latam.transfer.acl.comarch.entities;

import lombok.Data;

@Data
public class PointBalance {
	String expirationDate;
	String pointType;
	String pointTypeName;
	Integer points;
	String status;
	String statusName;
}
