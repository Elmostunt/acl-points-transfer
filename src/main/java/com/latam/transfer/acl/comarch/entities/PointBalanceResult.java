package com.latam.transfer.acl.comarch.entities;

import lombok.Data;

@Data
public class PointBalanceResult {
	String pointType;
	Integer points;
}
