package com.latam.transfer.acl.comarch.entities;

import java.util.Date;

import lombok.Data;
@Data
public class UsedCoupon {

	String couponNumber;
	String type;
	String status;
	String useResult;
	Integer usedQuantity;
	Date expiryDate;
	Boolean unlimitedUse;
}
