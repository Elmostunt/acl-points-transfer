package com.latam.transfer.acl.commons;

public class GlobalConstants {

	public static final String HEADER_X_APP_NAME = "x-application-name";
	public static final String HEADER_X_APP_VALUE = "LATAM";
	public static final String HEADER_TK1 = "tk1";
	public static final String HEADER_TK2 = "tk2";
	public static final String HEADER_X_API_KEY = "x-api-key";

	// POST VAR GENERIC
	public static final String DATA = "data";
	public static final String ROLID = "rolId";
	public static final String ROLNAME = "nameRol";
	public static final String CC = "CC";
	

	// UTILS HEX AND ETC
	public static final String EMPTY = "";
	public static final String DOT = ".";
	public static final String SLASH = "/";
	public static final String REX_DOT = "\\.";
	public static final String SEMICOLON = ";";
	public static final String REX_SEMICOLON = "\\;";
	public static final String ARROBA = "@";

	// TOKEN SERVICES
	public static final String DATAFORMAT = "dataFormat";
	public static final String DATAFORMAT_VALUE = "cc";
	public static final String POST_TOKENIZE = "/tokenize/v1/token";
	public static final String POST_UNTOKENIZE = "/tokenize/v1/data";
	public static final String POST_TOKENIZEBATCH = "/tokenize/v1/token/batch";
	public static final String POST_UNTOKENIZEBATCH = "/tokenize/v1/data/batch";
	
    public static final String STATUS_OK = "1";
    public static final String STATUS_ERROR = "0";	

    
    public static final String AUTHORIZATION = "Authorization";
    public static final String BEARER ="Bearer ";
    
	private GlobalConstants() {
	}
	
	
}