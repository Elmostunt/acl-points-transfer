package com.latam.transfer.acl.commons;

public enum ServiceStatusCode {
	OK(0, "Successful Operation"), 
	OK_WITH_FAILURES(1, "Operation Finalized with failures"), 
	ERROR(-1, "Not cataloged error"), 
	ERROR_PARAMETERS(-2, "Input parameters error"),
	BUSINESS_ERROR(-3, "Business error"), 
	CONNECT_TIMEOUT(-4, "Timeout error"), 
	ERROR_HEADERS(-5, "Input Header error"),
	ERROR_NO_DATA(-6, "No data result"), 
	ERROR_SESSION(-7, "Session with problem or invalid"),
	ERROR_APPLICATION_EXCEPTION(-1,"accumulationpoints.error.default.message"),
    SUCCESS(0,"accumulationpoints.sucess.default.message"),
    ERROR_NULL_REQUEST(1, "accumulationpoints.error.request.null"),
    ERROR_INVALID_PARAM_REQUEST(2, "accumulationpoints.error.invalidParamRequest"),
    //Application

    //WS SOAP ERROR CODE
    APPLICATIONUTILS_PROTOCOL(101,"accumulationpoints.error.ProtocolException.message"),
    APPLICATIONUTILS_JAXBEXCEPTION(106,"accumulationpoints.error.JAXBException.message"),
    //WS SOAP ERROR CODE

    //MultiplusConfig ERROR CODE
    MULTIPLUS_SUBMIT_TECNICAL_ERROR(12,"accumulationpoints.error.multiplus.tecnicalError.message"),
    MULTIPLUS_ERROR_FAULT_MSG(17,"accumulationpoints.error.multiplus.faultMsg.message"),
    MULTIPLUS_ERROR_WS_EXCEPTION(18,"accumulationpoints.error.wsException.message"),
    //MultiplusConfig ERROR CODE

    //SwitchflyConfig ERROR CODE
    SWITCHFLY_API_CALL(5,"accumulationpoints.exception.switchfly.message"),
    SWITCHFLY_API_EXCEPTION_DATE_NULL(6,"accumulationpoints.error.switchfly.param.date.null")
    //SwitchflyConfig ERROR CODE
    ;
	
	
	public final int CODE;
	public final String MESSAGE;

	ServiceStatusCode(int i, String message) {
		this.CODE = i;
		this.MESSAGE = message;
	}

	public int getCode() {
		return this.CODE;
	}

	public String getMessage() {
		return this.MESSAGE;
	}

	public static String getMessageByCode(int code) {
		String msg = "INVALID_STATUS";
		for (ServiceStatusCode data : ServiceStatusCode.values()) {
			if (data.getCode() == code) {
				msg = data.getMessage();
				break;
			}
		}
		return msg;
	}
}