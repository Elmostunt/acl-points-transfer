package com.latam.transfer.acl.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.latam.transfer.acl.comarch.entities.BalanceRS;
import com.latam.transfer.acl.comarch.entities.CorpProfile;
import com.latam.transfer.acl.comarch.entities.DirectRedemptionRQ;
import com.latam.transfer.acl.comarch.entities.DirectRedemptionRS;
import com.latam.transfer.acl.services.ComarchService;
import com.latam.transfer.acl.tokens.ComarchGenToken;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(value = "/commercial/corporate/loyalty/transfer-points/v1/acl" + "/comarch")
public class ComarchController {
	@Autowired
	ComarchService comarchSvc;
	@Autowired
	ComarchGenToken genToken;

	//Functional Test OK
	@GetMapping(value = "/balance/{accountId}")
	public ResponseEntity<BalanceRS> balance(@PathVariable String accountId) {
		try {
			String token = genToken.generate();
			return comarchSvc.display(Integer.valueOf(accountId), token);
		} catch (Exception e) {
		    log.error("[Comarch Balance Error]");
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	//Functional test OK
	@GetMapping(value = "/profile/{identifierId}")
	public ResponseEntity<CorpProfile> getData(@PathVariable String identifierId) {
		try {
			String token = genToken.generate();
			return comarchSvc.getData(identifierId,token);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	//Functional test OK
	@PostMapping(value = "/directredemption/{customerId}")
	public ResponseEntity<DirectRedemptionRS> process(@PathVariable String customerId,
			@RequestBody DirectRedemptionRQ directRedeemRq) {
		try {
			String token = genToken.generate();
			DirectRedemptionRS directRedeemRS = comarchSvc.redeem(directRedeemRq, token, customerId);
			log.info("Redeemed points = " + directRedeemRq.getPoints());
			return new ResponseEntity<>(directRedeemRS, HttpStatus.OK);
		} catch (Exception e) {
			log.error("[Comarch Direct Redemption Error] " + e.getMessage());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping(value = "/accept/{transactionId}")
	public HttpStatus accept(@PathVariable Integer transactionId) {
		try {
		    String token = genToken.generate();
		    log.info("Accept transaction - "+transactionId);
			return comarchSvc.accept(transactionId.toString(),token);
		} catch (Exception e) {
			return HttpStatus.NOT_FOUND;
		}
	}

	@GetMapping(value = "/reject/{transactionId}")
	public HttpStatus reject(@PathVariable Integer transactionId) {
		try {	
		    String token = genToken.generate();
	          log.info("Reject transaction - "+transactionId);
			return  comarchSvc.reject(transactionId.toString(),token);
		} catch (Exception e) {
			return HttpStatus.NOT_FOUND;
		}

	}

}
