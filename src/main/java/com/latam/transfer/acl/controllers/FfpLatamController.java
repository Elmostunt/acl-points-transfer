package com.latam.transfer.acl.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.latam.transfer.acl.ffp.entities.AccrualRQ;
import com.latam.transfer.acl.ffp.entities.AccrualRS;
import com.latam.transfer.acl.ffp.entities.FfpMember;
import com.latam.transfer.acl.services.FfpLatamService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@RequestMapping(value = "/commercial/corporate/loyalty/transfer-points/v1/acl" + "/ffp")
public class FfpLatamController {

	@Autowired
	FfpLatamService ffpLatamSvc;
	
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE ,path = "/accrual/{programId}/{memberId}")
	public ResponseEntity<AccrualRS> process(@PathVariable String programId, @PathVariable String memberId, @RequestBody AccrualRQ request) {
		log.info("Consulta para programID = " + programId);
		log.info("MemberId = " + memberId);
		ResponseEntity<AccrualRS> accrualResponse = ffpLatamSvc.accrual(programId, memberId, request);
		return accrualResponse;
	}
	
	@GetMapping(value="/member/{memberId}")
	public ResponseEntity<FfpMember[]> getMember(@PathVariable("memberId") String memberId){
		return  ffpLatamSvc.getMemberData(memberId);
	}
	
}
