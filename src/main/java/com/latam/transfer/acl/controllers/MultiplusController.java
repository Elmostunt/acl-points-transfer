package com.latam.transfer.acl.controllers;


import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.latam.transfer.acl.commons.ServiceStatusCode;
import com.latam.transfer.acl.entities.ServiceStatusDTO;
import com.latam.transfer.acl.exceptions.ApplicationException;
import com.latam.transfer.acl.services.MultiplusService;
import com.latam.webservices.multiplus.submeterloteacumulo.ObjectFactory;

import com.latam.webservices.multiplus.submeterloteacumulo.LoteAcumulo;
import com.latam.webservices.multiplus.submeterloteacumulo.PropriedadesExecucao;
import com.latam.webservices.multiplus.submeterloteacumulo.SubmeterLoteAcumuloInput;
import com.latam.webservices.multiplus.submeterloteacumulo.SubmeterLoteAcumuloOutput;
import com.latam.webservices.multiplus.submeterloteacumulo.LoteAcumulo.Canal;
import com.latam.webservices.multiplus.submeterloteacumulo.LoteAcumulo.ItensLote;
import com.latam.webservices.multiplus.submeterloteacumulo.LoteAcumulo.Lote;
import com.latam.webservices.multiplus.submeterloteacumulo.LoteAcumulo.Parceiro;
import com.latam.webservices.multiplus.submeterloteacumulo.LoteAcumulo.ItensLote.ItemLoteAcumulo;
import com.latam.webservices.multiplus.submeterloteacumulo.LoteAcumulo.ItensLote.ItemLoteAcumulo.ItemLote;
import com.latam.webservices.multiplus.submeterloteacumulo.LoteAcumulo.ItensLote.ItemLoteAcumulo.TransacaoAcumulo;
import com.latam.webservices.multiplus.submeterloteacumulo.LoteAcumulo.ItensLote.ItemLoteAcumulo.TransacaoAcumulo.ContextoParceiroTransacao;
import com.latam.webservices.multiplus.submeterloteacumulo.LoteAcumulo.ItensLote.ItemLoteAcumulo.TransacaoAcumulo.Participante;
import com.latam.webservices.multiplus.submeterloteacumulo.LoteAcumulo.ItensLote.ItemLoteAcumulo.TransacaoAcumulo.ProdutoFidelidade;
import com.latam.webservices.multiplus.submeterloteacumulo.LoteAcumulo.ItensLote.ItemLoteAcumulo.TransacaoAcumulo.TotalTransacao;
import com.latam.webservices.multiplus.submeterloteacumulo.LoteAcumulo.Parceiro.DadosCadastraisPj;

import lombok.extern.slf4j.Slf4j;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(value = "/commercial/corporate/loyalty/transfer-points/v1/acl" +"/multiplus")
public class MultiplusController{

    private MultiplusService service;

    @Autowired
    public MultiplusController(MultiplusService service) {
        this.service = service;
    }
    
    
    @PostMapping(value="/lot")
    @CrossOrigin(origins = "*")
    public ResponseEntity<SubmeterLoteAcumuloOutput> submitLotAccumulation(@RequestBody() SubmeterLoteAcumuloInput submitLotAccumulationRQ) {
       try {
           SubmeterLoteAcumuloOutput response = service.submitLotAccumulation(submitLotAccumulationRQ);
           return  new ResponseEntity<>(response,HttpStatus.OK);

        } catch (ApplicationException e) {
            log.error("An error ocurred in MultiplusAccumulationController.submitLotAccumulating", e.getMessage());
        }
        return null;
    }
    
    @GetMapping(value="/lot/test", produces = MediaType.APPLICATION_XML_VALUE)
    @CrossOrigin(origins = "*")
    public ResponseEntity<SubmeterLoteAcumuloOutput> submitLotAccumulationTest() throws DatatypeConfigurationException {
       try {
           
           
           SubmeterLoteAcumuloInput submitLotAccumulationRQ = createMultiplusAcumuloRQ();
           System.out.println("Request "+ submitLotAccumulationRQ);
           SubmeterLoteAcumuloOutput response = service.submitLotAccumulation2(submitLotAccumulationRQ);
           return new ResponseEntity<>(response,HttpStatus.OK);

        } catch (ApplicationException e) {
            log.error("An error ocurred in MultiplusAccumulationController.submitLotAccumulating", e.getMessage());
        }
        return null;
    }

    
    private SubmeterLoteAcumuloInput createMultiplusAcumuloRQ() throws DatatypeConfigurationException {
        ObjectFactory factory = new ObjectFactory();

        SubmeterLoteAcumuloInput request = factory.createSubmeterLoteAcumuloInput();

        
        LoteAcumulo loteAcumulo = new LoteAcumulo();

            Lote lote = new Lote();
            lote.setIdExternoLote("6");
            lote.setTamanhoLote(Long.valueOf(1));
            loteAcumulo.setLote(lote);

            Parceiro parceiro = new Parceiro();
            DadosCadastraisPj dadosCadastrais = new DadosCadastraisPj();
    //      dadosCadastrais.setCnpj("116739406302");
            dadosCadastrais.setCnpj("02012862000160");
            parceiro.setDadosCadastraisPj(dadosCadastrais);
            loteAcumulo.setParceiro(parceiro);

            ItensLote itensLote = new ItensLote();
                ItemLoteAcumulo itemLoteAcumulo = new ItemLoteAcumulo();
                    ItemLote itemLote = new ItemLote();
                    itemLote.setIdExternoItemLote("6");
                itemLoteAcumulo.setItemLote(itemLote);
                    TransacaoAcumulo transacaoAcumulo = new TransacaoAcumulo();
                        TotalTransacao totalTransacao = new TotalTransacao();
                        totalTransacao.setPontos(Long.valueOf(1));
                        transacaoAcumulo.setTotalTransacao(totalTransacao);
                        Date date = new Date();
                        GregorianCalendar c = new GregorianCalendar();
                        c.setTime(date);
                        XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
                        transacaoAcumulo.setDataHoraOrigemTransacao(date2);
                        transacaoAcumulo.setDescricaoTransacao("Creditos da compra no Estabelecimento X, NF 123456");
                        Participante participante = new Participante();
                            participante.setNumeroMultiplus("34400627160");
                        transacaoAcumulo.setParticipante(participante);
                        ProdutoFidelidade produtoFidelidade = new ProdutoFidelidade();
                            produtoFidelidade.setIdProduto("1-4M0B01E");
                         transacaoAcumulo.setProdutoFidelidade(produtoFidelidade);
                        ContextoParceiroTransacao contextoParceiroTransacao = new ContextoParceiroTransacao();
                            contextoParceiroTransacao.setIdTransacaoParceiro("3720485");
                        transacaoAcumulo.setContextoParceiroTransacao(contextoParceiroTransacao);
                itemLoteAcumulo.setTransacaoAcumulo(transacaoAcumulo);
            itensLote.getItemLoteAcumulo().add(itemLoteAcumulo);
        loteAcumulo.setItensLote(itensLote);
            
        Canal canal = new Canal();
        loteAcumulo.setCanal(canal);
        loteAcumulo.setItensLote(itensLote);
        request.setLoteAcumulo(loteAcumulo);

        PropriedadesExecucao value = new PropriedadesExecucao();
        value.setIdInterface(1);
        request.setPropriedadesExecucao(value);
        log.info("request" + request.toString());
        return request;
    }
    
//    public ServiceStatusDTO convertRequest(String request, Class objectCast) throws ApplicationException {
//
//        ServiceStatusDTO serviceStatusDTO = new ServiceStatusDTO();
//
//        if (request == null) {
//            serviceStatusDTO.setCode(ServiceStatusCode.ERROR_NULL_REQUEST.CODE);
//            serviceStatusDTO.setMessage(ServiceStatusCode.ERROR_NULL_REQUEST.MESSAGE);
//        }
//
//        try {
//            ObjectMapper mapper = new ObjectMapper();
//            serviceStatusDTO.setData(mapper.readValue(request, objectCast));
//            serviceStatusDTO.setCode(ServiceStatusCode.SUCCESS.CODE);
//        } catch (JsonParseException | JsonMappingException e) {
//            log.error("JsonParseException | JsonMappingException in process request", e.getMessage());
//            throw new ApplicationException("accumulationpoints.exception.converterequest.jsonparseexception", e);
//        } catch (IOException e) {
//            log.error("IOException in process request", e.getMessage());
//            throw new ApplicationException("accumulationpoints.exception.converterequest.ioexception", e);
//        }
//
//        return serviceStatusDTO;
//    }
}