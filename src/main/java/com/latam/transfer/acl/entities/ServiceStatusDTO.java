package com.latam.transfer.acl.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ServiceStatusDTO<T> implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;


    private int code;
    private String message;
    private String messageDetail;
    private T data;
    private String request;
    private String response;
}