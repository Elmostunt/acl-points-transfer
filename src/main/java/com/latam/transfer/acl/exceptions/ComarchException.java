package com.latam.transfer.acl.exceptions;

import java.io.Serializable;

public class ComarchException  extends Exception implements Serializable {


	private static final long serialVersionUID = -1L;
	
	public ComarchException() {
	}

	public ComarchException(String message) {
		super(message);
	}

	public ComarchException(Throwable cause) {
		super(cause);
	}

	public ComarchException(String message, Throwable cause) {
		super(message, cause);
	}
}
