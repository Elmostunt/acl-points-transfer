package com.latam.transfer.acl.exceptions;

public class ConfirmTransactionException extends Exception {
private static final long serialVersionUID = -1L;
	
	public ConfirmTransactionException() {
	}

	public ConfirmTransactionException(String message) {
		super(message);
	}

	public ConfirmTransactionException(Throwable cause) {
		super(cause);
	}

	public ConfirmTransactionException(String message, Throwable cause) {
		super(message, cause);
	}
}
