package com.latam.transfer.acl.exceptions;

public class DirectRedemptionException extends Exception {

	private static final long serialVersionUID = -1L;
	
	public DirectRedemptionException() {
	}

	public DirectRedemptionException(String message) {
		super(message);
	}

	public DirectRedemptionException(Throwable cause) {
		super(cause);
	}

	public DirectRedemptionException(String message, Throwable cause) {
		super(message, cause);
	}
}
