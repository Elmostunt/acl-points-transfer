package com.latam.transfer.acl.exceptions;

import java.io.Serializable;

import com.latam.transfer.acl.commons.ServiceStatusCode;

import lombok.Getter;
import lombok.Setter;

public class SessionException extends RuntimeException implements Serializable{


	private static final long serialVersionUID = -3784868492019148649L;

	@Getter
	@Setter
	private int code;

	@Getter
	@Setter
	private int nativeCode;

	@Getter
	@Setter
	private String nativeMessage;

	public SessionException(int code, String message) {
		super(message);
		this.code = code;
	}

	public SessionException(String message, String nativeMessage) {
		super(message);
		this.code = ServiceStatusCode.ERROR.getCode();
		this.nativeMessage = nativeMessage;
	}

	public SessionException(int code, String message, String nativeMessage) {
		super(message);
		this.code = code;
		this.nativeMessage = nativeMessage;
	}

	public SessionException(int code, int nativeCode, String message, String nativeMessage) {
		super(message);
		this.code = code;
		this.nativeMessage = nativeMessage;
		this.nativeCode = nativeCode;
	}
}
