package com.latam.transfer.acl.ffp.entities;

import lombok.Data;

@Data
public class AccrualRQ {

	Integer milles;
	String location;
	Product product;
}
