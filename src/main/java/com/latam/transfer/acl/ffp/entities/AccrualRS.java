package com.latam.transfer.acl.ffp.entities;

import lombok.Data;

@Data
public class AccrualRS {
	String statusCode;
	String number;
}
