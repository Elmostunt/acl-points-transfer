package com.latam.transfer.acl.ffp.entities;

import lombok.Data;

@Data
public class FfpMember {	
	String id;
	String name;
}

