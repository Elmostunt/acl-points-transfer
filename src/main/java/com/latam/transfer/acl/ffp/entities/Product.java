package com.latam.transfer.acl.ffp.entities;

import lombok.Data;

@Data
public class Product {

	String code;
	String name;
}
