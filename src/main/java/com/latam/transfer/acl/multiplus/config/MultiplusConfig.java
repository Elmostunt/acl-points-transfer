package com.latam.transfer.acl.multiplus.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component
@ConfigurationProperties(prefix="multiplus")
public class MultiplusConfig {
    private List<NameValue> headers;

    //Services values
    private Service identificarParticipante;
    private Service submeterLoteAcumulo;
    private Service consultarLoteAcumulo;

}
