package com.latam.transfer.acl.multiplus.config;

import lombok.Data;

@Data
public class NameValue {
    private String name;
    private String value;
}
