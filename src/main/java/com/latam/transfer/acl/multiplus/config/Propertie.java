package com.latam.transfer.acl.multiplus.config;

import lombok.Data;

@Data
public class Propertie {

    Integer[] idInterface;
    String cnpjPartner;

}
