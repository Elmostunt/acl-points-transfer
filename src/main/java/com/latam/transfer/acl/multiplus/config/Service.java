package com.latam.transfer.acl.multiplus.config;

import lombok.Data;

@Data
public class Service {
    private String user;
    private String password;
    private String endpoint;
    private Propertie propertie;
    private String version;
}