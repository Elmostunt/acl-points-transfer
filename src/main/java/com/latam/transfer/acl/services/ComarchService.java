package com.latam.transfer.acl.services;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.latam.transfer.acl.comarch.entities.BalanceRS;
import com.latam.transfer.acl.comarch.entities.CorpProfile;
import com.latam.transfer.acl.comarch.entities.DirectRedemptionRQ;
import com.latam.transfer.acl.comarch.entities.DirectRedemptionRS;
import com.latam.transfer.acl.exceptions.ComarchException;

public interface ComarchService {

	public HttpStatus accept(String transactionId, String token) throws ComarchException;	
	public HttpStatus reject(String transactionId, String token) throws ComarchException;
	public ResponseEntity<BalanceRS> display(Integer accountId, String token) throws ComarchException;
	public DirectRedemptionRS redeem(DirectRedemptionRQ directRedeemRq, String token, String customerId) throws ComarchException;
	ResponseEntity<CorpProfile> getData(String identifierId, String token) throws ComarchException;

}
