package com.latam.transfer.acl.services;

import org.springframework.http.ResponseEntity;

import com.latam.transfer.acl.ffp.entities.AccrualRQ;
import com.latam.transfer.acl.ffp.entities.AccrualRS;
import com.latam.transfer.acl.ffp.entities.FfpMember;

public interface FfpLatamService {
	
	public ResponseEntity<FfpMember[]> getMemberData(String memberId);

    ResponseEntity<AccrualRS> accrual(String programId, String memberId, AccrualRQ request);
}
