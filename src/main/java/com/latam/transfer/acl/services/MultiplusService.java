package com.latam.transfer.acl.services;

import com.latam.transfer.acl.entities.ServiceStatusDTO;
import com.latam.transfer.acl.exceptions.ApplicationException;
import com.latam.webservices.multiplus.submeterloteacumulo.SubmeterLoteAcumuloInput;
import com.latam.webservices.multiplus.submeterloteacumulo.SubmeterLoteAcumuloOutput;

public interface MultiplusService {

//	ServiceStatusDTO identifyParticipant(String id, Integer idInterface) throws ApplicationException;
//
//	ServiceStatusDTO identifyParticipantByCpf(String cpf) throws ApplicationException;
//
//	ServiceStatusDTO identifyParticipantById(String id) throws ApplicationException;

	SubmeterLoteAcumuloOutput submitLotAccumulation(SubmeterLoteAcumuloInput input) throws ApplicationException;

    SubmeterLoteAcumuloOutput submitLotAccumulation2(SubmeterLoteAcumuloInput input) throws ApplicationException;

}
 