package com.latam.transfer.acl.services;

public interface SessionServices {

	Boolean validateSession(String applicationName, String tkn1);

}
