package com.latam.transfer.acl.services.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.latam.transfer.acl.comarch.entities.BalanceRS;
import com.latam.transfer.acl.comarch.entities.CorpProfile;
import com.latam.transfer.acl.comarch.entities.DirectRedemptionRQ;
import com.latam.transfer.acl.comarch.entities.DirectRedemptionRS;
import com.latam.transfer.acl.commons.GlobalConstants;
import com.latam.transfer.acl.exceptions.ComarchException;
import com.latam.transfer.acl.services.ComarchService;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@Service
public class ComarchServicesImpl implements ComarchService {

	@Value("${ws.balance.endpoint}")
	String balanceEndpoint;

	@Value("${ws.comarch.accept.trx}")
	String acceptTrxEndpoint;

	@Value("${ws.comarch.reject.trx}")
	String rejectTrxEndpoint;

	@Value("${ws.comarch.profile.data}")
	String profileDataEndpoint;
	
	@Value("${ws.comarch.dr.endpoint}")
	String directRedemptionEndpoint;

	@Override
	public ResponseEntity<CorpProfile> getData(String identifierId, String token) throws ComarchException {
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.add(GlobalConstants.AUTHORIZATION, GlobalConstants.BEARER +token);
			
			HttpEntity<String> entity = new HttpEntity<>(headers);
			RestTemplate restTemplate = new RestTemplate();
			String endpoint = profileDataEndpoint.replace("{identifierId}", identifierId);

			return restTemplate.exchange(endpoint, HttpMethod.GET, entity,
					CorpProfile.class);
		} catch (Exception e) {
			throw new ComarchException(e.getMessage(), e.getCause());
		}
	}

	@Override
	public ResponseEntity<BalanceRS> display(Integer accountId, String token) throws ComarchException {
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.set(GlobalConstants.AUTHORIZATION, GlobalConstants.BEARER + token);
			HttpEntity<Object> entity = new HttpEntity<>(headers);
			String endpoint = balanceEndpoint.replace("{accountId}", String.valueOf(accountId));
			RestTemplate restTemplate = new RestTemplate();
			return restTemplate.exchange(endpoint, HttpMethod.GET, entity, BalanceRS.class);
		} catch (Exception e) {
			throw new ComarchException(e.getMessage(), e.getCause());
		}
	}

	@Override
	public DirectRedemptionRS redeem(DirectRedemptionRQ directRedeemRq, String token, String customerId) throws ComarchException {
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.set(GlobalConstants.AUTHORIZATION, GlobalConstants.BEARER +token);
			HttpEntity<DirectRedemptionRQ> entity = new HttpEntity<>(directRedeemRq,headers);
			String endpoint = directRedemptionEndpoint.replace("{customerId}", customerId);
			RestTemplate restTemplate = new RestTemplate();
			return restTemplate.postForObject(endpoint, entity, DirectRedemptionRS.class);
		}catch (Exception e) {
			throw new ComarchException(e.getMessage(), e.getCause());
		}

	}

	public HttpStatus accept(String transactionId,String token) throws ComarchException {
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.set(GlobalConstants.AUTHORIZATION, GlobalConstants.BEARER  + token);
			HttpEntity<Object> entity = new HttpEntity<>(headers);
			String endpoint = acceptTrxEndpoint.replace("{transactionId}", transactionId);
			log.info("EndpointAccept"+ endpoint);
			RestTemplate restTemplate = new RestTemplate();
			return restTemplate.postForObject(endpoint, entity, HttpStatus.class);
		} catch (Exception e) {
			throw new ComarchException(e.getMessage(), e.getCause());
		}
	}

	public HttpStatus reject(String transactionId,String token) throws ComarchException {
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.set(GlobalConstants.AUTHORIZATION, GlobalConstants.BEARER  + token);
			HttpEntity<Object> entity = new HttpEntity<>(headers);
			String endpoint = rejectTrxEndpoint.replace("{transactionId}", transactionId);
			RestTemplate restTemplate = new RestTemplate();
			return restTemplate.postForObject(endpoint, entity, HttpStatus.class);
		} catch (Exception e) {
			throw new ComarchException(e.getMessage(), e.getCause());
		}
	}
}
