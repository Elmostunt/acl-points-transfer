package com.latam.transfer.acl.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.latam.transfer.acl.ffp.entities.AccrualRQ;
import com.latam.transfer.acl.ffp.entities.AccrualRS;
import com.latam.transfer.acl.ffp.entities.FfpMember;
import com.latam.transfer.acl.services.FfpLatamService;
import com.latam.transfer.acl.tokens.FfpLatamGenToken;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@Service
public class FfpLatamServiceImpl implements FfpLatamService{

	@Value("${ws.accrual.endpoint}")
	String accrualEndpoint;
	
	@Value("${ws.member.data.endpoint}")
	String memberDataEndpoint;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	FfpLatamGenToken genFfpLatamToken;
	
	@Override
	public ResponseEntity<AccrualRS> accrual(String programId, String memberId, AccrualRQ request) {
		try {
			String token = genFfpLatamToken.generate("accrual-create");
			HttpHeaders headers = new HttpHeaders();
			
			headers.add("Authorization", "Bearer "+token);
			headers.add("Content-type", MediaType.APPLICATION_JSON_VALUE);
			String endpoint=(accrualEndpoint.replace("{programId}", String.valueOf(programId))).replace("{memberId}", String.valueOf(memberId));
	        HttpEntity<Object> requestFfp = new HttpEntity<>(headers);
			log.info("[FFP ACCUAL] Endpoint -> " + endpoint );
			return restTemplate.postForEntity(endpoint, requestFfp, AccrualRS.class);
		}catch(Exception e) {
		    log.error("Exception" + e);
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
	}

	@Override
	public ResponseEntity<FfpMember[]> getMemberData(String memberId) {
		try {	
			String token = genFfpLatamToken.generate("member-show");
			UriComponentsBuilder builder = UriComponentsBuilder.newInstance().scheme("https")
					.host("test.api.latam-pass.latam.com").path("programs").queryParam("member-id", memberId);
			log.info("Endpoint "+ builder.toUriString());
			HttpHeaders headers = new HttpHeaders();
			headers.set("Authorization", "Bearer "+token);
			HttpEntity<Object> request = new HttpEntity<>(headers);
			return restTemplate.exchange(builder.toUriString(),HttpMethod.GET,request,FfpMember[].class);
		}catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

	}

}
