package com.latam.transfer.acl.services.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jws.soap.SOAPBinding;
import javax.xml.soap.SOAPElement;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.handler.MessageContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.latam.transfer.acl.services.MultiplusService;
import com.latam.transfer.acl.entities.ServiceStatusDTO;
import com.latam.transfer.acl.multiplus.config.MultiplusConfig;
import com.latam.transfer.acl.multiplus.config.NameValue;
import com.latam.transfer.acl.commons.ServiceStatusCode;
import com.latam.transfer.acl.exceptions.ApplicationException;
import com.latam.webservices.multiplus.identificarparticipante.IdentificarParticipanteInput;
import com.latam.webservices.multiplus.identificarparticipante.IdentificarParticipanteOutput;
import com.latam.webservices.multiplus.identificarparticipante.IdentificarParticipantev1;
import com.latam.webservices.multiplus.identificarparticipante.IdentificarParticipantev1PortType;
import com.latam.webservices.multiplus.submeterloteacumulo.ErroTecnicoFaultMsg;
import com.latam.webservices.multiplus.submeterloteacumulo.SubmeterLoteAcumuloFaultMsg;
import com.latam.webservices.multiplus.submeterloteacumulo.SubmeterLoteAcumuloInput;
import com.latam.webservices.multiplus.submeterloteacumulo.SubmeterLoteAcumuloOutput;
import com.latam.webservices.multiplus.submeterloteacumulo.SubmeterLoteAcumulov1;
import com.latam.webservices.multiplus.submeterloteacumulo.SubmeterLoteAcumulov1PortType;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MultiplusServiceImpl implements MultiplusService{
	
    @Autowired
	private MultiplusConfig config;

    @Autowired
    private RestTemplate restTemplate;
    
    @Override
    public SubmeterLoteAcumuloOutput submitLotAccumulation(SubmeterLoteAcumuloInput input) throws ApplicationException {
//              com.latam.webservices.multiplus.submeterloteacumulo.PropriedadesExecucao propExec = new com.latam.webservices.multiplus.submeterloteacumulo.PropriedadesExecucao();
//        propExec.setIdInterface(config.getSubmeterLoteAcumulo().getPropertie().getIdInterface()[0]);

//        input.setPropriedadesExecucao(propExec);
        SubmeterLoteAcumulov1PortType portType = (SubmeterLoteAcumulov1PortType) getPortType("https://api.pontosmultiplus.com.br/sandbox/API/SubmeterLoteAcumuloAPISv1?wsdl", new SubmeterLoteAcumulov1().getSubmeterLoteAcumulov1());
//      SubmeterLoteAcumulov1PortType portType = (SubmeterLoteAcumulov1PortType) getPortType(config.getSubmeterLoteAcumulo().getEndpoint(), new SubmeterLoteAcumulov1().getSubmeterLoteAcumulov1());

        log.info("portType "+portType);
        try {
            return portType.submeterLoteAcumulo(input);
        } catch (com.latam.webservices.multiplus.submeterloteacumulo.ErroTecnicoFaultMsg e){
            log.error("ErroTecnicoFaultMsg to call IdentificarParticipanteAPISV1", e);
            throw new ApplicationException("");
        } catch (SubmeterLoteAcumuloFaultMsg e) {
            log.error("SubmeterLoteAcumuloFaultMsg to call IdentificarParticipanteAPISV1", e);
            throw new ApplicationException("");
        } catch (WebServiceException e) {
            log.error("WebServiceException to call IdentificarParticipanteAPISV1", e);
            throw new ApplicationException("");
        } catch (Exception e) {
            log.error("WebServiceException to call IdentificarParticipanteAPISV1", e);
            throw new ApplicationException("");
        }

    }


    @Override
    @SOAPBinding
    public SubmeterLoteAcumuloOutput submitLotAccumulation2(SubmeterLoteAcumuloInput input) throws ApplicationException {
       HttpHeaders header = new HttpHeaders();
       header.add("access_token", "7a56071e-134c-31ec-b7fa-32ad0dfefa24");
       header.add("client_id", "98e52f2e-5ccd-3cf4-9f67-cb7c083d2bd5");
       header.setContentType(MediaType.APPLICATION_XML);
       HttpEntity<SubmeterLoteAcumuloInput> requestMultiplus = new HttpEntity<>(input,header);

        SubmeterLoteAcumuloOutput response =  restTemplate.postForObject("https://api.pontosmultiplus.com.br/sandbox/API/SubmeterLoteAcumuloAPISv1", requestMultiplus, SubmeterLoteAcumuloOutput.class);
        return response;

    }
    private Object getPortType(String endPoint, Object portType) {
        BindingProvider provider = (BindingProvider) portType;
        Map<String, Object> req_ctx = provider.getRequestContext();
        req_ctx.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPoint);

        Map<String, List<String>> headers = new HashMap<>();

        for(NameValue nameValue: config.getHeaders()) {
            headers.put(nameValue.getName(), Collections.singletonList(nameValue.getValue()));
        }

        req_ctx.put(MessageContext.HTTP_REQUEST_HEADERS, headers);

        return portType;
    }

}
