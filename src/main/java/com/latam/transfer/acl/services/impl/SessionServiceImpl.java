package com.latam.transfer.acl.services.impl;
//package com.latam.transfer.services.impl;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.MediaType;
//import org.springframework.stereotype.Service;
//import org.springframework.web.client.RestTemplate;
//import org.springframework.web.util.UriComponentsBuilder;
//
//import com.latam.transfer.commons.ServicesStatusCodes;
//import com.latam.transfer.exceptions.SessionException;
//import com.latam.transfer.services.SessionServices;
//import com.latam.transfer.session.entities.SessionTkn1;
//
//@Service
//public class SessionServiceImpl implements SessionServices{
//
//	@Value("${ws.session.schema}")
//	private String gapisSchema;
//
//	@Value("${ws.session.endpoint}")
//	private String gapisEndpoint;
//
//	@Value("${ws.session.path}")
//	private String gapisPath;
//
//	@Value("${ws.session.paramtkn}")
//	private String gapisParamtkn;
//
//	@Autowired
//	private RestTemplate restTemplate;
//
//	public Boolean validateSession(String applicationName, String tk1){
//
//		if (null != applicationName || null != tk1) {
//			try {
//				// Crear peticion
//				UriComponentsBuilder builder = UriComponentsBuilder.newInstance().scheme(gapisSchema)
//						.host(gapisEndpoint).path(gapisPath).queryParam(gapisParamtkn, tk1);
//				// Crear header
//				HttpHeaders httpRequest = new HttpHeaders();
//				httpRequest.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
//
//				SessionTkn1 gApiResult = restTemplate.postForObject(builder.toUriString(), httpRequest,
//						SessionTkn1.class);
//				if (null != gApiResult.getUser_id()) {
//					return Boolean.TRUE;
//				} else {
//					return Boolean.FALSE;
//				}
//			} catch (Exception e) {
//				throw new SessionException(ServicesStatusCodes.ERROR_SESSION.getCode(),
//						ServicesStatusCodes.ERROR_SESSION.getMessage(), "invalidSession");
//			}
//		} else {
//			return Boolean.FALSE;
//		}
//	}
//
//}
