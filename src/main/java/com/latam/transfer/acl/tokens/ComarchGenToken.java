package com.latam.transfer.acl.tokens;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.latam.transfer.acl.tokens.entities.TokenComarch;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ComarchGenToken {

	@Value("${ws.comarch.tkn.endpoint}")
	String comarchTokenEndpoint;
	@Value("${ws.comarch.token}")
	String comarchTkn;
	@Value("${ws.comarch.tkn.clientid}")
	String clientIdValue;
	@Value("${ws.comarch.tkn.clientsecret}")
	String clientSecretValue;
	@Value("${ws.comarch.tkn.granttype}")
	String grantTypeValue;
	@Value("${ws.comarch.tkn.scope}")
	String scopeValue;
	
	@Autowired
    RestTemplate restTemplate;
	
	public String generate() {
		try {

			MultiValueMap<String, String> qParamsMap = new LinkedMultiValueMap<>();
			qParamsMap.add("client_id", clientIdValue);
			qParamsMap.add("client_secret", clientSecretValue);
			qParamsMap.add("grant_type", grantTypeValue);
			qParamsMap.add("scope", scopeValue);

			// Calling DAL Service from Kubernetes Cluster
			UriComponentsBuilder builder = UriComponentsBuilder.newInstance()
					// base path
					.scheme("https").host("api-latam-qa.clm-comarch.com").path("b2b/login").queryParams(qParamsMap);

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set("Authorization", "Basic " + comarchTkn);
			
			HttpEntity<Object> entity = new HttpEntity<>(headers);
			String endpoint = builder.toUriString();
			log.info("URI "+endpoint);

			ResponseEntity<TokenComarch> response = restTemplate.postForEntity(endpoint, entity, TokenComarch.class);
			return response.getBody().getAccess_token();
		} catch (Exception e) {
			log.error("Error on get token" + e.getMessage());
		}
		return "";
	}
}
