package com.latam.transfer.acl.tokens;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.latam.transfer.acl.tokens.entities.FfpLatamToken;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class FfpLatamGenToken {

	@Value("${ws.latam.ffp.granttype}")
	private String grantTypeValue;

	@Value("${ws.latam.ffp.clientid}")
	private String clientIdValue;
	
	@Value("${ws.latam.ffp.clientsecret}")
	private String clientSecretValue;
	
	@Value("${ws.latam.ffp.scope.membershow}")
	private String scopeValueMemberShow;
	
	@Value("${ws.latam.ffp.scope.accrualcreate}")
	private String scopeValueAccrualCreate;
	
	@Autowired
	RestTemplate restTemplate;
	
	
	public String generate(String scope) {
		try {

			MultiValueMap<String, String> qParamsMap = new LinkedMultiValueMap<>();
			qParamsMap.add("client_id", clientIdValue);
			qParamsMap.add("client_secret", clientSecretValue);
			qParamsMap.add("grant_type", grantTypeValue);
			if(scope.equalsIgnoreCase(scopeValueAccrualCreate)) {
				qParamsMap.add("scope", scopeValueAccrualCreate);
			}else {
				qParamsMap.add("scope", scopeValueMemberShow);
			}

			// Calling DAL Service from Kubernetes Cluster
			UriComponentsBuilder builder = UriComponentsBuilder.newInstance().scheme("https").host("test.api.latam-pass.latam.com").path("oauth/token");

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(qParamsMap,headers);
			String endpoint = builder.toUriString();
			log.info("URI " + endpoint);
			ResponseEntity<FfpLatamToken> response = restTemplate.postForEntity(endpoint, entity, FfpLatamToken.class);
			return response.getBody().getAccess_token();
		} catch (Exception e) {
			log.error("Error on get token" + e.getMessage());
		}
		return "";
	}
}

