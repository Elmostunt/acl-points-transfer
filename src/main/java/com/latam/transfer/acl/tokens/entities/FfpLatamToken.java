package com.latam.transfer.acl.tokens.entities;

import lombok.Data;

@Data
public class FfpLatamToken {

    String access_token;
    String token_type;
    Integer expires_in;
    String scope;
    	
}
