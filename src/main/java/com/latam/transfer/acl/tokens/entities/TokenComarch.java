package com.latam.transfer.acl.tokens.entities;

import lombok.Data;

@Data
public class TokenComarch {
	String access_token;
	String token_type;
	String expires_in;
	String scope;
	String jti;
}
