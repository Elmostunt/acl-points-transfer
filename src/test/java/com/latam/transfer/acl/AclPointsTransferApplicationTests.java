package com.latam.transfer.acl;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.latam.transfer.acl.controllers.FfpLatamController;
import com.latam.transfer.acl.ffp.entities.FfpMember;
import com.latam.transfer.acl.tokens.ComarchGenToken;
import com.latam.transfer.acl.tokens.FfpLatamGenToken;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@Slf4j
public class AclPointsTransferApplicationTests {

	@Autowired
	FfpLatamController ffpController;
	
	private static String urlApi = "http://localhost:8081/commercial/corporate/loyalty/transfer-points/v1/acl/";
	private static String memberPath = "member/";
	private String memberId = "18470384K";
	
	@Before
	public void fillData() {
		
	}
	@Test
	public void contextLoads() {
		
		RestTemplate restTemplate = new RestTemplate(); 
		try {
			String endpoint = urlApi+memberPath+memberId;
			log.info("endpoint "+endpoint);
			ResponseEntity<FfpMember[]> response= restTemplate.getForEntity(endpoint,FfpMember[].class);
			assertNotNull(response.getBody());
		}catch(Exception e){
			log.error("error on test");
		}
		
		try {
			String endpoint = urlApi+memberPath+"18470384K";
			ResponseEntity<FfpMember[]> response= restTemplate.getForEntity(endpoint,FfpMember[].class);
			assertNotNull(response.getBody());
		}catch(Exception e) {
			log.error("error on test");
		}
	}
	
	

}
