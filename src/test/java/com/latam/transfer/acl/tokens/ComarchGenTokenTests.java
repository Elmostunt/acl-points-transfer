package com.latam.transfer.acl.tokens;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.latam.transfer.acl.tokens.entities.TokenComarch;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@RunWith(SpringRunner.class)
public class ComarchGenTokenTests {

//    @Autowired
//    ComarchGenToken genToken;
    
    @Mock
    private RestTemplate restTemplate;
    
    @InjectMocks
    ComarchGenToken genToken = new ComarchGenToken();
    
    String endpoint;
    String token;
    String clientIdValue;
    String clientSecretValue;
    String grantTypeValue;
    String scopeValue;
    MultiValueMap<String, String> qParamsMap = new LinkedMultiValueMap<>();
    HttpHeaders headers = new HttpHeaders();


    @Before
    public void init() {
        token = "YXBpOlVJTU5TaXpDeW15ZEw3aEQ=";
        clientIdValue = "api";
        clientSecretValue = "UIMNSizCymydL7hD";
        grantTypeValue = "client_credentials"; 
        scopeValue = "any";
    
        ReflectionTestUtils.setField(genToken,"comarchTkn",token);
        ReflectionTestUtils.setField(genToken,"clientIdValue",clientIdValue);
        ReflectionTestUtils.setField(genToken,"clientSecretValue",clientSecretValue);
        ReflectionTestUtils.setField(genToken,"grantTypeValue",grantTypeValue);
        ReflectionTestUtils.setField(genToken,"scopeValue",scopeValue);
        UriComponentsBuilder builder = UriComponentsBuilder.newInstance()
                .scheme("https").host("api-latam-qa.clm-comarch.com").path("b2b/login").queryParams(qParamsMap);
        endpoint = builder.toUriString();
        qParamsMap.add("client_id", clientIdValue);
        qParamsMap.add("client_secret", clientSecretValue);
        qParamsMap.add("grant_type", grantTypeValue);
        qParamsMap.add("scope", scopeValue);
        
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Basic " + token);
    }
    
    @Test
    public void comarchTokenTest() {
        
        TokenComarch comarchTkn = new TokenComarch();
        comarchTkn.setAccess_token("token");
        comarchTkn.setExpires_in("10");
        comarchTkn.setJti("3456");
        comarchTkn.setToken_type("ok");
        
        ResponseEntity<TokenComarch> tokenComarch = new ResponseEntity<TokenComarch>(comarchTkn, HttpStatus.OK);
        HttpEntity<Object> entity = new HttpEntity<>(headers);
        log.info("Endpoint"+endpoint+"entity"+entity);
        when(restTemplate.postForEntity(endpoint, entity, TokenComarch.class)).thenReturn(tokenComarch);
        String acc_tkn= genToken.generate();
        assertNotNull(acc_tkn);
    }
    
}
