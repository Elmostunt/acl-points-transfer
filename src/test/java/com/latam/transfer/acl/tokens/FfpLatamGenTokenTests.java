package com.latam.transfer.acl.tokens;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.latam.transfer.acl.tokens.entities.FfpLatamToken;



@RunWith(SpringRunner.class)
public class FfpLatamGenTokenTests {

    @InjectMocks 
    FfpLatamGenToken ffpToken;
    
    @Mock
    RestTemplate restTemplate;
    
    private String grantTypeValue;
    private String clientIdValue;    
    private String clientSecretValue;    
    private String scopeValueMemberShow;
    private String scopeValueAccrualCreate;
    private String endpoint;
    MultiValueMap<String, String> qParamsMap = new LinkedMultiValueMap<>();
    HttpHeaders headers = new HttpHeaders();

    @Before
    public void fillData() {

     grantTypeValue="client_credentials";
     clientIdValue="LATAM_CORPORATE";
     clientSecretValue="3coo5KKPz2a8vuVsccM3mx7dOXA2wthJuSD2bPVm";
     scopeValueMemberShow="member-show";
     scopeValueAccrualCreate="accrual-create";
 
     ReflectionTestUtils.setField(ffpToken,"grantTypeValue",grantTypeValue);
     ReflectionTestUtils.setField(ffpToken,"clientIdValue",clientIdValue);
     ReflectionTestUtils.setField(ffpToken,"clientSecretValue",clientSecretValue);
     ReflectionTestUtils.setField(ffpToken,"scopeValueMemberShow",scopeValueMemberShow);
     ReflectionTestUtils.setField(ffpToken,"scopeValueAccrualCreate",scopeValueAccrualCreate);
    }
    
    @Test 
    public void ffpTokenTest() {
        
      qParamsMap.add("client_id", clientIdValue);
      qParamsMap.add("client_secret", clientSecretValue);
      qParamsMap.add("grant_type", grantTypeValue);
      qParamsMap.add("scope", scopeValueAccrualCreate);
      UriComponentsBuilder builder = UriComponentsBuilder.newInstance().scheme("https").host("test.api.latam-pass.latam.com").path("oauth/token");
      headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
      HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(qParamsMap,headers);
      
      
      FfpLatamToken tkn = new FfpLatamToken();
      tkn.setAccess_token("ASD");
      tkn.setExpires_in(1);
      tkn.setScope("accrual-create");
      tkn.setToken_type("OK");
      ResponseEntity<FfpLatamToken> responseFfpToken = new ResponseEntity<FfpLatamToken>(tkn,HttpStatus.OK);
      endpoint = builder.toUriString();
        //Test con member-show
       when(restTemplate.postForEntity(endpoint, entity, FfpLatamToken.class)).thenReturn(responseFfpToken);
       assertNotNull(ffpToken.generate(scopeValueAccrualCreate));

//       //Test con accrual-create
//        String tokenAccrual = ffpToken.generate(scopeValueAccrualCreate);
//        assertNotNull(tokenAccrual);
//        
//        try {
//            String scopeNull="";
//            String token = ffpToken.generate(null);
//        }catch(Exception e) {
//            assertNotNull(e);
//        }
//        
    }
    
    @Test 
    public void ffpTokenTest2() {
        
      qParamsMap.add("client_id", clientIdValue);
      qParamsMap.add("client_secret", clientSecretValue);
      qParamsMap.add("grant_type", grantTypeValue);
      qParamsMap.add("scope", scopeValueMemberShow);
      UriComponentsBuilder builder = UriComponentsBuilder.newInstance().scheme("https").host("test.api.latam-pass.latam.com").path("oauth/token");
      headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
      HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(qParamsMap,headers);
      
      
      FfpLatamToken tkn = new FfpLatamToken();
      tkn.setAccess_token("ASD");
      tkn.setExpires_in(1);
      tkn.setScope("accrual-create");
      tkn.setToken_type("OK");
      ResponseEntity<FfpLatamToken> responseFfpToken = new ResponseEntity<FfpLatamToken>(tkn,HttpStatus.OK);
      endpoint = builder.toUriString();
        //Test con member-show
       when(restTemplate.postForEntity(endpoint, entity, FfpLatamToken.class)).thenReturn(responseFfpToken);
       assertNotNull(ffpToken.generate(scopeValueMemberShow));

//       //Test con accrual-create
//        String tokenAccrual = ffpToken.generate(scopeValueAccrualCreate);
//        assertNotNull(tokenAccrual);
//        
//        try {
//            String scopeNull="";
//            String token = ffpToken.generate(null);
//        }catch(Exception e) {
//            assertNotNull(e);
//        }
//        
    }
    
    
    
}
